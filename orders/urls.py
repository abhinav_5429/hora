from django.urls import re_path
from orders.apis import CartDetails, OrdersList, OrderDetails


urlpatterns = [
    re_path(r'^(?P<order_id>\d+)/', OrderDetails.as_view(), name='order-details'),
    re_path(r'^cart/', CartDetails.as_view(), name='cart'),
    re_path(r'^cart/(?P<item_id>\d+)/', CartDetails.as_view(), name='cart-items'),
    re_path(r'', OrdersList.as_view(), name='orders'),

]


