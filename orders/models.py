from django.db import models
from django.conf import settings

# Create your models here.


class UserCart(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='cart')
    total_value = models.DecimalField(max_digits=19, decimal_places=2, default=0)
    total_items = models.SmallIntegerField(default=0)
    products = models.ManyToManyField('products.Product', through='orders.CartItems')


class CartItems(models.Model):
    cart = models.ForeignKey(UserCart, related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey('products.Product', on_delete=models.CASCADE)
    quantity = models.SmallIntegerField(default=1)
    added_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)


class Order(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    total_value = models.DecimalField(max_digits=19, decimal_places=2, default=0)
    products = models.ManyToManyField('products.Product', through='orders.OrderItems')
    ordered_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    delivered = models.BooleanField(default=False)


class OrderItems(models.Model):
    order = models.ForeignKey('orders.Order', on_delete=models.CASCADE)
    product = models.ForeignKey('products.Product', on_delete=models.CASCADE)
    quantity = models.SmallIntegerField(default=1)
    modified_on = models.DateTimeField(auto_now=True)