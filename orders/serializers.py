from rest_framework import serializers
from orders.models import Order, OrderItems, CartItems


class CartItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartItems
        fields = ('product', 'quantity', 'modified_on', 'added_on')


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItems
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ('items', 'products', 'delivered')

    def get_items(self, instance):
        print(instance)
        queryset = OrderItems.objects.filter(order=instance)
        serializer = OrderItemSerializer(queryset, many=True)
        return serializer.data
