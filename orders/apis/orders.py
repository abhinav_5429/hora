from django.db import transaction
from rest_framework import status
from rest_framework import views
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import generics
from rest_framework import mixins

from orders.models import UserCart, CartItems, Order, OrderItems
from orders.serializers import CartItemsSerializer, OrderSerializer
from products.models import Product


class OrderDetails(generics.GenericAPIView,
                   mixins.RetrieveModelMixin):

    lookup_url_kwarg = 'order_id'
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        kwargs = self.kwargs
        product_id = kwargs.get('order_id', None)
        queryset = Order.objects.filter(user=self.request.user)
        return queryset

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class OrdersList(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        orders = Order.objects.select_related('products').filter(user=request.user).order('title')
        serializer = OrderSerializer(orders, many=True)
        response = {'orders': serializer.data}
        return Response(data=response, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        # use items from cart to create order
        response = dict()
        try:
            cart = UserCart.objects.get(user=request.user)
        except UserCart.DoesNotExist:
            response['error'] = 'Cart does not exist'
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)

        cart_items = CartItems.objects.select_related('product').filter(cart=cart)
        if not cart_items.exists():
            response['error'] = 'No product in cart. Add to cart first.'
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)

        order = Order(user=request.user)
        order_items = []
        for item in cart_items:
            order_item = OrderItems(product=item.product, quantity=item.quantity)
            order.total_value += int(order_item.quantity) * order_item.product.price
            order_items.append(order_item)

        try:
            with transaction.atomic():
                order.save()
                for item in order_items:
                    item.order_id = order.id
                OrderItems.objects.bulk_create(order_items)
                cart_items.delete()
        except Exception as e:
            print(e)
            response['error'] = 'Error Creating order. Please try again later'
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = OrderSerializer(order)
            response['order'] = serializer.data
        return Response(data=response, status=status.HTTP_201_CREATED)




