from django.db import transaction
from rest_framework import status
from rest_framework import views
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import generics
from rest_framework import mixins

from orders.models import UserCart, CartItems
from orders.serializers import CartItemsSerializer
from products.models import Product


class CartDetails(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user = request.user
        response = dict()
        try:
            cart = user.cart
            items = cart.items.all()
            serializer = CartItemsSerializer(items, many=True)
            response['items'] = serializer.data
        except UserCart.DoesNotExist:
            response['items'] = []

        return Response(data=response, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        user = request.user
        response = dict()
        try:
            cart = user.cart
        except UserCart.DoesNotExist:
            cart = UserCart(user=user)
            cart.save()

        product_id = request.data.get('product_id')
        quantity = request.data.get('quantity', 1)
        if not product_id:
            response['error'] = 'Id of product to be added is missing'
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)

        try:
            quantity = int(quantity)
        except ValueError:
            response['error'] = 'Quantity has to be an integer'
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)

        try:
            product = Product.objects.get(pk=product_id)
        except Product.DoesNotExist:
            response['error'] = 'Id of product to be added is missing'
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)

        cart_item = CartItems(cart=user.cart, product=product, quantity=quantity)
        cart.total_items += quantity
        cart.total_value += product.price * quantity
        try:
            with transaction.atomic():
                cart_item.save()
                cart.save()
        except Exception as e:
            response['error'] = 'Error adding item to cart'
            return Response(data=response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            serializer = CartItemsSerializer(cart)
            response['item'] = serializer.data

        return Response(data=response, status=status.HTTP_200_OK)


class CartItemDetails(generics.GenericAPIView,
                      mixins.UpdateModelMixin,
                      mixins.DestroyModelMixin):

    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CartItems
    lookup_url_kwarg = 'item_id'

    def get_queryset(self):
        user = self.request.user
        cart = user.cart
        queryset = CartItems.objects.filter(cart=cart)
        return queryset