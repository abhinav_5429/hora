from django.contrib.auth import authenticate, login
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView


class Login(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        email = request.data.get('email', None)
        password = request.data.get('password', None)
        response = dict()

        if not email:
            response['error'] = 'Email is missing'
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)

        if not password:
            response['error'] = 'Password is missing'
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(request, email=email, password=password)
        if user:
            login(request, user)
            response['user'] = user.to_dict()
        else:
            response['error'] = 'Invalid login credentials'
            return Response(data=response, status=status.HTTP_403_FORBIDDEN)

        return Response(data=response, status=status.HTTP_200_OK)