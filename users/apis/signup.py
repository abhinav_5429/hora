import re

from django.db import transaction
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from users.models import User


class Signup(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        response = {}
        signup_data, errors = self.validate_signup_data(request)
        if errors:
            response['error'] = {
                'message': 'Validation failed on signup data',
                "errors": errors
            }
            return Response(data=response, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        if User.objects.filter(email=signup_data['email']).exists():
            response['error'] = 'User Already exists for given email'
            return Response(data=response, status=status.HTTP_200_OK)

        user = User(**signup_data)
        try:
            with transaction.atomic():
                user.set_password(signup_data['password'])
                user.save()
        except Exception as e:
            response['error'] = 'Unable to signup. Please try again after sometime'
            return Response(data=response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            response['user'] = user.to_dict()
            return Response(data=response, status=status.HTTP_201_CREATED)

    @staticmethod
    def validate_signup_data(request):
        email_pattern = r'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$'
        signup_data, errors = {}, []
        email = request.data.get('email', None)
        if email:
            if re.match(pattern=email_pattern, string=email):
                signup_data['email'] = re.match(email_pattern, email).group()
            else:
                errors.append('Invalid email %s' % email)
        else:
            errors.append('Email is missing', None)

        password = request.data.get('password', None)
        if not password:
            errors.append('Passwords missing', None)
        else:
            signup_data['password'] = password

        name = request.data.get('name', None)
        if name:
            signup_data['name'] = request.data.get('name', None)
        else:
            errors.append('Name is missing')

        return signup_data, errors
