from django.urls import re_path
from users.apis import Signup, Login


urlpatterns = [
    re_path(r'^signup/', Signup.as_view(), name='signup'),
    re_path(r'^login/', Login.as_view(), name='login'),
]
