from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser

# Create your models here.


class UserManager(BaseUserManager):
    """
    Manager for auth model.
    """
    def create_user(self, email, password=None):
        """
        Create user
        :param phone: phone no of the user being created (country code included)
        :param password: password of the user if provided
        :return: User object created
        """
        user = self.model(email=email)
        if password:
            user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email)
        user.set_password(password)
        user.is_superuser = True
        user.save()
        return user


class User(AbstractBaseUser):
    name = models.CharField(max_length=255, default='')
    email = models.EmailField(unique=True)
    is_superuser = models.BooleanField(help_text='Determines whether the user has super user privileges',
                                       default=False)
    joined_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(help_text='Designates whether this user should be treated as active.',
                                    default=True)

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'

    objects = UserManager()

    def __str__(self):
        return self.name

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
        }