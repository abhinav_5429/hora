from django.urls import re_path
from products.apis import ProductList, CategoryList, ReviewList,ProductDetails, AllProducts


urlpatterns = [
    re_path(r'^categories/', CategoryList.as_view(), name='categories'),
    re_path(r'^(?P<category_id>\d+)/products-list/', ProductList.as_view(), name='products'),
    re_path(r'^(?P<product_id>\d+)/', ProductDetails.as_view(), name='product-details'),
    re_path(r'^(?P<product_id>\d+)/reviews/', ReviewList.as_view(), name='reviews'),
    re_path(r'^all-products/', AllProducts.as_view(), name='all-products')
]


