from rest_framework import generics
from rest_framework import mixins
from rest_framework import permissions
from django.shortcuts import get_object_or_404
from products.models import Product, ProductCategory
from products.serilalizers import ProductSerializer


class ProductList(generics.ListCreateAPIView):
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        kwargs = self.kwargs
        category_id = kwargs.get('category_id', None)
        category = get_object_or_404(ProductCategory, pk=category_id)
        queryset = Product.objects.filter(category_id=category_id).order_by('-average_rating')
        return queryset


class ProductDetails(generics.GenericAPIView,
                     mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin):

    serializer_class = ProductSerializer
    lookup_url_kwarg = 'product_id'
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def get_queryset(self):
        kwargs = self.kwargs
        product_id = kwargs.get('product_id', None)
        queryset = Product.objects.filter(pk=product_id)
        return queryset


class AllProducts(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ProductSerializer

    def get_queryset(self):
        return Product.objects.all()
