from rest_framework import generics
from rest_framework import mixins
from django.shortcuts import get_object_or_404
from rest_framework import permissions

from products.models import ProductReview, Product
from products.serilalizers import ReviewSerializer


class ReviewList(generics.ListCreateAPIView, mixins.DestroyModelMixin):
    serializer_class = ReviewSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def get_queryset(self):
        product_id = self.kwargs.get('product_id')
        product = get_object_or_404(Product, pk=product_id)
        queryset = ProductReview.objects.filter(product_id=product_id).order_by('-rating')
        return queryset





