from rest_framework import generics
from rest_framework import mixins
from rest_framework import permissions

from products.models import ProductCategory
from products.serilalizers import CategorySerializer


class CategoryList(generics.ListCreateAPIView, mixins.DestroyModelMixin):
    serializer_class = CategorySerializer
    queryset = ProductCategory.objects.all().order_by('sequence_no')
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)



