from django.db import models

# Create your models here.


class ProductCategory(models.Model):
    title = models.CharField(max_length=255)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    sequence_no = models.SmallIntegerField(default=1)

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=255)
    category = models.ForeignKey(ProductCategory, related_name='products', on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True, default=None)
    average_rating = models.DecimalField(max_digits=3, decimal_places=2, null=True, blank=True, default=None)
    images = models.TextField(null=True, blank=True, default=None, help_text='Comma separated image url strings')
    price = models.DecimalField(max_digits=19, decimal_places=2)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class ProductReview(models.Model):
    """
    Saves reviews of a product. Images provided are assumed to be array of urls for simplicity
    """
    rating = models.SmallIntegerField(default=None, null=False)  # rating of product between 4 and 5
    review = models.TextField()
    images = models.TextField(null=True, blank=True, default=None, help_text='Comma separated image url strings')
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

